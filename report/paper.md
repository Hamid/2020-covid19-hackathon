---
title: 'Packaging Scientific Software for Debian'
tags:
  - COVID-19
  - Debian
  - DebianMed
authors:
  - name: Michael R. Crusoe
    orcid: 0000-0002-2961-9670
    affiliation: 1, 2
  - name: Andreas Tille
    orcid: 0000-0000-0000-0000
    affiliation: 3
  - name: Rebecca N. Palmer
  - name: Étienne Mollier
  - name: Olek Wojnar
  - name: Andrius Merkys
    orcid: 0000-0002-7731-6236
    affiliation: 4
  - name: Dylan Aïssi
    orcid: 0000-0002-5858-6468
    affiliation: 5
affiliations:
 - name: Vrije Universiteit, Amsterdam, The Netherlands
   index: 1
 - name: Curii Corporation, Somerville, MA, USA
   index: 2
 - name: Institution 1, address, city, country
   index: 3
 - name: Vilnius University Institute of Biotechnology, Vilnius, Lithuania
   index: 4
 - name: INSERM U1219, Bordeaux Population Health Research Center, University of Bordeaux, Bordeaux, France
   index: 5
date: 2020-04-12
bibliography: paper.bib
---

# Introduction

Debian is ...; the Debian Med team is ..

(link to announcement email)

We participated in the 2020 COVID-19 Biohackathon from April 5-11.

Add to this section a couple of paragraphs introducing the work done dring the BioHackathon, CodeFest, VoCamp or Sprint event. Please add references whenever needed, for instance [@Katayamaetal-2010].

Please separate paragraphs with a double line.

# Scientific software relicensed as F/OSS

1. r-cran-locfit [recently freed in COVID-19 effort and uploaded to Debian](https://tracker.debian.org/pkg/r-cran-locfit)
   This has enormous effects prominent reverse dependencies like r-bioc-deseq2 and its reverse dependencies like the PIGZ single-cell sequencing packages - we should list them here
1. gmap [APACHE-2.0 licensed recently](https://tracker.debian.org/pkg/gmap)
1. Work in progres: [blat](https://github.com/ucscGenomeBrowser/kent/issues/32) (Any help to convince upstream is really appreciated)

# Newly packaged software

1. Andreas Tille
    * [augur](https://blends.debian.org/med/tasks/covid-19#augur)
    * [chip-seq](https://blends.debian.org/med/tasks/covid-19#chip-seq)
    * [wtdbg2](https://blends.debian.org/med/tasks/covid-19#wtdbg2)
    * [r-cran-mediana](https://blends.debian.org/med/tasks/covid-19#r-cran-mediana)
    * [r-bioc-rgsepd](https://blends.debian.org/med/tasks/covid-19#r-bioc-rgsepd)
    * [r-bioc-pwmenrich](https://blends.debian.org/med/tasks/covid-19#r-bioc-pwmenrich)
    * [r-cran-covid19us](https://blends.debian.org/med/tasks/covid-19#r-cran-covid19us)
    * [r-bioc-mutationalpatterns](https://blends.debian.org/med/tasks/covid-19#r-bioc-mutationalpatterns)
    * [r-bioc-tcgabiolinks](https://blends.debian.org/med/tasks/covid-19#r-bioc-tcgabiolinks)
    * [r-bioc-rots](https://tracker.debian.org/r-bioc-rots)
    * [sumalibs](https://tracker.debian.org/pkg/sumalibs) in support of [sumaclust](https://blends.debian.org/med/tasks/covid-19#sumaclust)
    * Several preconditions for GNU R packages above: [r-cran-fingerprint](https://tracker.debian.org/r-cran-fingerprint), [r-cran-rcdklibs](https://tracker.debian.org/r-cran-rcdklibs), [r-cran-rsvg](https://tracker.debian.org/r-cran-rsvg), [r-bioc-chemminer](https://tracker.debian.org/r-bioc-chemminer), [r-bioc-fmcsr](https://tracker.debian.org/r-bioc-fmcsr), [r-bioc-genelendatabase](https://tracker.debian.org/rbioc-genelendatabase), [r-bioc-goseq](https://tracker.debian.org/r-bioc-goseq), [r-bioc-pwmenrich](https://tracker.debian.org/r-bioc-pwmenrich), [r-bioc-wrench](https://tracker.debian.org/r-bioc-wrench), [r-bioc-gosemsim](https://tracker.debian.org/r-bioc-gosemsim), [r-cran-hash](https://tracker.debian.org/r-cran-hash), [r-bioc-bladderbatch](https://tracker.debian.org/r-bioc-bladderbatch), [r-bioc-deseq](https://tracker.debian.org/r-bioc-deseq), [r-bioc-edaseq](https://tracker.debian.org/r-bioc-edaseq), [r-bioc-edger](https://tracker.debian.org/r-bioc-edger), [r-bioc-mutationalpatterns](https://tracker.debian.org/r-bioc-mutationalpatterns), [r-bioc-sva](https://tracker.debian.org/r-bioc-sva), [r-cran-exactranktests](https://tracker.debian.org/r-cran-exactranktests), [r-cran-ggpubr](https://tracker.debian.org/r-cran-ggpubr), [r-cran-gridsvg](https://tracker.debian.org/r-cran-gridsvg), [r-cran-ggsignif](https://tracker.debian.org/r-cran-ggsignif), [r-cran-grimport2](https://tracker.debian.org/r-cran-grimport2), [r-cran-km.ci](https://tracker.debian.org/r-cran-km.ci), [r-cran-kmsurv](https://tracker.debian.org/r-cran-kmsurv), [r-cran-polynom](https://tracker.debian.org/r-cran-polynom), [r-cran-gganimate](https://tracker.debian.org/r-cran-gganimate), [r-cran-maxstat](https://tracker.debian.org/r-cran-maxstat), [r-cran-pkgcond](https://tracker.debian.org/r-cran-pkgcond), [r-cran-parsetools](https://tracker.debian.org/r-cran-parsetools), [r-  cran-postlogic](https://tracker.debian.org/r-cran-postlogic), [r-cran-testextra](https://tracker.debian.org/r-cran-testextra), [r-cran-transformr](https://tracker.debian.org/r-cran-transformr), [r-cran-survmisc](https://tracker.debian.org/r-cran-survmisc), [r-cran-purrrogress](https://tracker.debian.org/r-cran-purrrogress), [r-cran-av](https://tracker.debian.org/r-cran-av), [r-cran-survminer](https://tracker.debian.org/r-cran-survminer),
1. Alexandre Mestiashvili
   * [orthanc-python](https://tracker.debian.org/pkg/orthanc-python)
1. Michael R. Crusoe
   * [routine-update](http://tracker.debian.org/routine-update)
1. Sao I Kuan
   * [recan](https://blends.debian.org/med/tasks/covid-19#recan)
1. Pranav Ballaney
   * [trinculo](https://blends.debian.org/med/tasks/covid-19#trinculo)
1. Nilesh Patra
   * [parasail](https://blends.debian.org/med/tasks/covid-19#parasail) - Ready in salsa
1. Andrius Merkys
   * JS dependencies of streamlit: [node-cuint](https://tracker.debian.org/pkg/node-cuint), [node-xxhashjs](https://tracker.debian.org/pkg/node-xxhashjs) (waiting in NEW)
   * JS dependencies of shiny-server: [node-ip-address](https://tracker.debian.org/pkg/node-ip-address), [node-stable](https://tracker.debian.org/pkg/node-stable)
1. Dylan Aïssi
    * [r-cran-isoband](https://tracker.debian.org/r-cran-isoband)

# Bugs fixed

1. Andreas Tille
    * [Bug 813821: pynn: FTBFS: TypeError: 'float' object cannot be interpreted as an index](https://bugs.debian.org/813821)
    * [Bug 953052: psychopy: python2 dependencies](https://bugs.debian.org/953052)
    * [Bug 955006: gubbins: fails to migrate to testing for too long: ftbfs on i386](https://bugs.debian.org/955006)
    * [Bug 955662: python-biom-format: FTBFS: ERROR: Failure: ModuleNotFoundError (No module named 'matplotlib')](https://bugs.debian.org/955662)
1. Antoni Villalonga
    * [Bug 943055: harvest-tools: Python2 removal in sid/bullseye](https://bugs.debian.org/943055)
    * [Bug 943280: sprai: Python2 removal in sid/bullseye](https://bugs.debian.org/943280)
    * [Bug 936802: kmer: Python2 removal in sid/bullseye](https://bugs.debian.org/936802)
1. Tony Mancill
    * [Bug 950311: fastqc: FAIL strings in output - what does this mean?](https://bugs.debian.org/950311)
    * [Bug 952603: python-tinyalign: license of buildwheels.sh is CC0](https://bugs.debian.org/952603)
1. Pierre Gruet
    * [Bug 952093: sumaclust: FTBFS: mtcompare_sumaclust.c:18:10: fatal error: libfasta/sequence.h: No such file or directory](https://bugs.debian.org/952093)
    * [Bug 954262: src:sumaclust: fails to migrate to testing for too long](https://bugs.debian.org/954262)
    * [Bug 956232: sumaclust: Last sequence of input is skipped on arm64](https://bugs.debian.org/956232)
1. Etienne Mollier
    * [Bug 952757: mypy: build errors in man page](https://bugs.debian.org/952757)
1. Pranav Ballaney
    * [Bug 954511: bioperl-run: FTBFS: dh_auto_test: error: perl Build test --verbose 1 returned exit code 255](https://bugs.debian.org/954511)
1. Rebecca N. Palmer
    * [Bug 955056: snakemake: FTBFS with Sphinx 2.4: AttributeError: 'member_descriptor' object has no attribute 'expandtabs'](https://bugs.debian.org/955056)
    * [snakemake: contains broken duplicate test](https://github.com/snakemake/snakemake/issues/322)
    * maybe [Bug 953970: python-boto: autopkgtest failure with Python 3.8 as default](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=953970)
1. Michael R. Crusoe
    * [Bug 955566: khmer: autopkgtest regression: undefined references](https://bugs.debian.org/955566)
1. Ben Tris and Steffen Möller
    * python-deeptools had dysfunctional d/watch file, package updated
1. Steffen Möller
    * ricks-amd-utils updated (that is for Folding@Home and its Covid-19 projects)
1. Liubov Chuprikova
    * scanned for the COVID/virology-related software, that is not in Debian, on Biohackathon wiki pages and in the scientific literature
    * [Bug 942404: discosnp: flaky autopkgtest: discoRes_k_31_c_auto_D_100_P_1_b_0_coherent.fa: No such file or directory](https://bugs.debian.org/942404)
1. Dylan Aïssi
    * [Bug 942469: r-cran-rmarkdown: directory to symlink conversion not correctly handled on upgrade](https://bugs.debian.org/942469)
    * [Bug 943550: r-cran-rmarkdown: autopkgtest regression: html5shiv.min.js: openBinaryFile: does not exist](https://bugs.debian.org/943550)
    * [Bug 955640: r-cran-uwot: autopkgtest regression: Index size is not a multiple of vector size](https://bugs.debian.org/955640)
    * [Bug 954813: r-cran-rlang breaks r-cran-ggplot2 autopkgtest: `out` not identical to `exp`](https://bugs.debian.org/954813)
    * [Bug 955167: r-cran-sf: autopkgtest regression](https://bugs.debian.org/955167)

# Debian infrastuctiure

1. Import autopkgtest results into UDD to have easy access to test results for all relevant packages

# Work in progress

For each entry: include what it is, link to the in-progress work (if available), who is working on it, and the status as of 2020-04-11 including any blockers to progress

1. QA page for Blends framework featuring autopkgtest, testing migrations, autoremovals due to bugs of dependencies and necessary source-only uploads
1. Sumana & Michael's prep work for writing grant proposals
1. [bazel](https://bugs.debian.org/782654) / [tensorflow](https://salsa.debian.org/science-team/tensorflow) - Olek Wojnar
    * Wanted by several potential packages, but previous attempts suggest they are difficult to package
    * Successfully engaged with Google team to start collaboration on bringing both into Debian
1. [streamlit](https://salsa.debian.org/science-team/streamlit)
    * tensorflow dependency can be dropped by skipping 2 tests
    * Other Python dependencies look manageable - [validators](https://bugs.debian.org/956082) packaged, 5 to go
    * But has over 300 (recursive) JavaScript dependencies
1. [snakemake](https://salsa.debian.org/med-team/snakemake) - Rebecca N. Palmer
    * Update to latest upstream version
    * General packaging cleanup
    * Despite the branch name, [conda](https://bugs.debian.org/926416) is still not a required dependency, but would be good to have
1. [prinseq-lite](https://salsa.debian.org/med-team/prinseq-lite/) - Étienne Mollier
    * New package ([upstream homepage](http://prinseq.sourceforge.net/) [ITP](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=956384))
    * PReprocessing and INformation of SEQuence data (lite version)
    * Requirement of [VirusSeeker](https://wupathlabs.wustl.edu/virusseeker/) which will be next on the list
1. [smart-open](https://salsa.debian.org/med-team/smart-open) - Sao I Kuan
    * New package ([upstream homepage](https://github.com/RaRe-Technologies/smart_open))
    * Utils for streaming the large files (HDFS, WebHDFS, HTTP, HTTPS, SFTP, or local filesystem)
    * Requirement of [idseq-bench]
1. [idseq-bench](https://salsa.debian.org/med-team/idseq-bench) - Sao I Kuan
    * New package ([upstream homepage](https://github.com/chanzuckerberg/idseq-bench))
    * Benchmark generator for the IDseq Portal
1. [idseq-dag](https://salsa.debian.org/med-team/idseq-dag) - Emmanuel Arias
    * New package ([upstream homepage](https://github.com/chanzuckerberg/idseq-dag))
    * Need pytz
1. [r-cran-rhandsontable](https://salsa.debian.org/r-pkg-team/r-cran-rhandsontable) - Dylan Aïssi
    * Regenerate numbro/languages.js

## Tables, figures and so on

Please remember to introduce tables (see Table 1) before they appear on the document. We recommend to center tables, formulas and figure but not the corresponding captions. Feel free to modify the table style as it better suits to your data.

Table 1
| Header 1 | Header 2 |
| -------- | -------- |
| item 1 | item 2 |
| item 3 | item 4 |

Remember to introduce figures (see Figure 1) before they appear on the document. 

![BioHackrXiv logo](./biohackrxiv.png)
 
Figure 1. A figure corresponding to the logo of our BioHackrXiv preprint.

# Other main section on your manuscript level 1

Feel free to use numbered lists or bullet points as you need.
* Item 1
* Item 2

Debian describes itself as a Universal Operating system. Firstly this is about
people, everyone is invited to join and shall feel welcome. It is also
about being available for many computer architectures [https://www.debian.org/ports/].
This has its limits, though, as for very small devices there are alternatives
like 'OpenWrt' [https://www.openwrt.org], a minimal Operating System tailored
for routers. Still, Debian is important also for working with such small images:
Using the OpenWrt SDK on Debian we prepared a package for BOINC for OpenWrt. The
scientific app for TN-GRID [http://gene.disi.unitn.it/test]
was compiled statically on Debian and the same binary
is now distributed for OpenWrt, Debian and other Linux distributions of the same
32 and 64bit ARM architectures. TN-GRID runs on a Debian derivative
to investige cause-effect relations also for Covid-19 associated genes.

# Discussion and/or Conclusion

The packages introduced during this sprint, and also all the less visible
routine maintenance of packages, no stands for itself. The more the Linux
distribution already offers, the less work is required to be performed at the
periphery to bring molecular workflows to life. A major form to distribute readily
usable workflows are containers or cloud instances. But also these need to be
maintained to keep pace with the fast scientific progress. These updates are performed
e.g. by executing dockerfiles - which can decide for themselves to either start 
from scratch, which is cumbersome with many dependencies, or to build upon what
Linux distributions provide [ref to workflow paper]. <some examples?>

# Future work

And maybe you want to add a sentence or two on how you plan to continue. Please keep reading to learn about citations and references.

For citations of references, we prefer the use of parenthesis, last name and year. If you use a citation manager, Elsevier – Harvard or American Psychological Association (APA) will work. If you are referencing web pages, software or so, please do so in the same way. Whenever possible, add authors and year. We have included a couple of citations along this document for you to get the idea. Please remember to always add DOI whenever available, if not possible, please provide alternative URLs. You will end up with an alphabetical order list by authors’ last name.

# Jupyter notebooks, GitHub repositories and data repositories

* Please add a list here
* Make sure you let us know which of these correspond to Jupyter notebooks. Although not supported yet, we plan to add features for them
* And remember, software and data need a license for them to be used by others, no license means no clear rules so nobody could legally use a non-licensed research object, whatever that object is

# Acknowledgements
Please always remember to acknowledge the BioHackathon, CodeFest, VoCamp, Sprint or similar where this work was (partially) developed.

Thanks a lot to Debian ftpmaster support for extremely fast processing of new packages.

# References

Leave this section blank, create a paper.bib with all your references.
